// 引入的Koa是一个构造函数，所以我们这里写成了大写
const Koa = require('koa');
// 引入并执行koa-router函数，一定要后面加括号
const router = require('koa-router')();
const static = require('koa-static');
// koa-views用来配置koa的模板引擎
const views = require('koa-views');
const nunjucks = require('nunjucks');
const session = require('koa-session');
const parser = require('koa-parser');
// 创建应用
const app = new Koa();
app.use(parser());

app.keys = ['123456'];
app.use(
  session(
    {
      maxAge: 10 * 1000,
    },
    app
  )
);

// 访问的时候直接localhost:3000/文件名就可以了，不需要加public
app.use(static(__dirname + '/public'));
app.use(
  views(__dirname + '/views', {
    // 将使用nunjucks模板引擎渲染html为后端的文件
    map: {
      html: 'nunjucks',
    },
  })
);
// 一定要把配置模板的代码放到配置路由的前面否则就会报错
app.use(router.routes());

// 首页：任何用户都可以访问
router.get('/', async (ctx) => {
  await ctx.render('home');
});

//登录页：任何用户都可以访问
router.get('/login', async (ctx) => {
  await ctx.render('login');
});

// 处理请求的路由
router.post('/login', async (ctx) => {
  let { username, password } = ctx.request.body;
  if (username === 'admin' && password === '123456') {
    ctx.session.user = 'admin';
    //重定向
    ctx.redirect('/list');
  } else {
    ctx.redirect('/');
  }
});

//内容页：只有登录成功才可以访问
router.get('/list', async (ctx) => {
  if (ctx.session.user) {
    await ctx.render('list');
  } else {
    ctx.redirect('/');
  }
});
// 注销
router.get('/logout', async (ctx) => {
  ctx.session.user = '';
  ctx.redirect('/');
});
//设置监听端口
app.listen(3000, (req, res) => {
  console.log('server is runing');
});
